
async function deleteUser(event) {
  const text = event.target.parentNode.firstElementChild.innerText;

  const username = text.substring(10);


  try {
    const response = await axios.delete(`/deleteUser/${username}`);
    if (response.status !== 500 && response.status !== 401 && response.status !== 403) {
      event.target.parentNode.remove();
    } else {
      alert(`Error:${response.status}`);
    }
  } catch (err) {
    alert(err);
  }
}

const deleteUserButtons = Array.from(document.getElementsByClassName('delete-button-user'));
deleteUserButtons.forEach((element) => {
  element.addEventListener('click', deleteUser);
});

async function changeRole(event) {
  const text = event.target.parentNode.firstElementChild.innerText;

  const username = text.substring(10);
  const userPermission = event.target.parentNode.children[1].innerText.substring(11);

  try {
    const response = await axios.post(`/userPermission/${username}`);
    const body = response.data;
    if (response.status !== 500 && response.status !== 400 && response.status !== 403) {
      if (userPermission === 'user') {
        event.target.parentNode.children[1].innerText = 'User role: admin';
        event.target.parentNode.getElementsByClassName('modify-button-user')[0].value = 'Demote to normal user';
        event.target.parentNode.getElementsByClassName('delete-button-user')[0].style.display = 'none';
      } else {
        event.target.parentNode.children[1].innerText = 'User role: user';
        event.target.parentNode.getElementsByClassName('modify-button-user')[0].value = 'Give admin role';
        event.target.parentNode.getElementsByClassName('delete-button-user')[0].style.display = 'block';
      }
    } else {
      alert(body.errorMessage);
    }
  } catch (err) {
    alert(err);
  }
}
const moduifyPermissionButton = Array.from(document.getElementsByClassName('modify-button-user'));
moduifyPermissionButton.forEach((element) => {
  element.addEventListener('click', changeRole);
});

async function deleteStudent(event) {
  const text = event.target.parentNode.firstElementChild.innerText;

  const studentID = text.substring(12);

  try {
    const response = await axios.delete(`/deleteStudent/${studentID}`);
    if (response.status !== 500 && response.status !== 401 && response.status !== 403) {
      event.target.parentNode.remove();
    } else {
      alert(`Error:${response.status}`);
    }
  } catch (err) {
    alert(err);
  }
}

const deleteStudentButtons = Array.from(document.getElementsByClassName('delete-button-student'));
deleteStudentButtons.forEach((element) => {
  element.addEventListener('click', deleteStudent);
});

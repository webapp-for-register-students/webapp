export function isNumeric(value) {
  return /^\d+$/.test(Number(value));
}

function notNullValues(studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade) {
  return (studentName && cnp && facultatea && specializare && numarMatricol && companyName && grade);
}

export function validNewStudent(studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade) {
  let ok = false;

  if (notNullValues(studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade)) {
    ok = true;
  }
  if (!isNumeric(numarMatricol) || (!isNumeric(grade)) || Number(grade) < 1 || Number(grade) > Number(10) )  {
    ok = false;
  }

  return ok;
}

import express from 'express';
import * as db from '../db/requests.js';
import * as deleteDb from '../db/deleteDb.js';
import * as insertDb from '../db/insertDb.js';
import {
  validNewStudent
} from '../validations/validation.js';

const router = express.Router();

router.get('/', async (req, resp) => {

  try {
    const message = '';
    const students = await db.allStudents(); 
    resp.render('main_page',{students, message});
  } catch (err) {
    resp.status(500).send(`Error: ${err.message}`);
  }
});


router.get('/users', async (req, resp) => {

  if (resp.locals.payload.username && resp.locals.permission === 'admin') {
    try {
      
      const users = await db.allUserWithRole(); 
      const message = '';
      resp.render('users', {
         users, message,  
      });
    } catch (err) {
      const message = 'Loading users failed!';
      const users = [];
      resp.status(500).render('users', { message, users });
    }
  } else {
    resp.status(403).send('Only the admin can see the users!');
  }
});

router.delete('/deleteUser/:username', async (req, resp) => {
  try {
    if (resp.locals.payload.username && resp.locals.permission === 'admin') {
      const userPermission = await db.userRole(req.params.username);
      if (userPermission !== 'admin') {

        await  deleteDb.deleteUser(req.params.username);
        resp.sendStatus(204);
      } else {
        resp.status(401).send('You can not deete an admin!');
      }
    } else {
      resp.status(403).send('Only admin can delete user!');
    }
  } catch (err) {
    resp.status(500).send(`Error: ${err.message}`);
  }
});

router.delete('/deleteStudent/:studentID', async (req, resp) => {
  try {
    if (resp.locals.payload.username ) {
        await  deleteDb.deleteStudent(req.params.studentID);
        resp.sendStatus(204);

    } else {
      resp.status(403).send('You need to login!');
    }
  } catch (err) {
    resp.status(500).send(`Error: ${err.message}`);
  }
});

router.post('/userPermission/:username', async (req, resp) => {
  try {
    if (resp.locals.payload.username && resp.locals.permission === 'admin') {
      const userPermission = await db.userRole(req.params.username);
      if (userPermission === 'admin') {
        const newUserRole = 'user';
        await db.userRoleModify(newUserRole, req.params.username);
        resp.sendStatus(204);
      } else {
        const newUserRole = 'admin';
        await db.userRoleModify(newUserRole, req.params.username);
        resp.sendStatus(204);
      }
    } else {
      resp.status(403).send('Only admin can modify roles!');
    }
  } catch (err) {
    resp.status(500).send(`Error while changing the roles! ${err.message}`);
  }   
});   

router.get('/new_student_post', async (req, resp) => {
  try {
    const message = ' ';
    resp.render('new_student_post',{message});
  } catch (err) { 
    resp.status(500).send(`Error : ${err.message}`);
  }
});

async function addNewStudent(req, resp) {
  const {
    studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade,
  } = req.body;
  const ok = validNewStudent(studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade);
  if (resp.locals.payload.username) {
    if (ok) {
      try {
        await insertDb.insertStudent(studentName, cnp, facultatea, specializare, numarMatricol, companyName, grade);
        const message = 'New student was added succesfully!';
        const students = await db.allStudents(); 
        resp.render('main_page',{students, message});
      } catch (err) {
        resp.status(500).send(`Error: ${err.message}`);
      }
    } else {
      resp.status(400).send('Error in input fields!');
    }
  } else {
    resp.status(403).send('You can add new student only if you are logged in!');
  }
}

router.post('/new_student_post', addNewStudent);

export default router;

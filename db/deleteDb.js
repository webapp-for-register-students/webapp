import dbConnection from './connection.js';

export const deleteUser = (username) => {
  const query = 'DELETE FROM Users WHERE Username = ?;';
  return dbConnection.executeQuery(query, username);
};

export const deleteStudent = (studentID) => {
  const query = 'DELETE FROM Students WHERE StudentID = ?;';
  return dbConnection.executeQuery(query, studentID);
};

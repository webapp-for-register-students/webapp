import express from 'express';
import cookieParser from 'cookie-parser';
import { createAllTable } from './db/createTables.js';
import requestRoutes from './routes/router.js';
import authRouter from './routes/authRouter.js';
import { decodeJWTToken } from './middleware/auth.js';

const app = express();
const port = process.env.PORT || 3000;

app.use('/static', express.static('static'));

app.use(express.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(decodeJWTToken);

app.use('/', requestRoutes);
app.use('/auth', authRouter);

createAllTable().then(() => {
  app.listen(port, () => { console.log('Server listening...'); });
});  

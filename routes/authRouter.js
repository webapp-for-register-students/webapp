/* eslint-disable no-param-reassign */
import { Router } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import  SECRET  from '../util/config.js';
import * as db from '../db/requests.js';
import * as insertDb from '../db/insertDb.js';

const router = Router();

const PASSWORD = '123456789';
const HASHED_PASSWORD = bcrypt.hashSync(PASSWORD, bcrypt.genSaltSync(10));

// Default admin user
setTimeout(async () => {
  const usersNumber = await db.UsersNumber();
  if (usersNumber === 0) {
    insertDb.insertUserAdmin('Admin', HASHED_PASSWORD);
  }
}, 2000);

router.post('/login', async (req, resp) => {
  const { username, password } =  req.body;
  if (username && password) {
    const passwordInDatabase = await db.passwordByUsername(username);
    if (passwordInDatabase) {
      if (await bcrypt.compare(password, passwordInDatabase.password)) {
        const token = jwt.sign({ username }, SECRET);
        resp.cookie('token', token, { httpOnly: true, sameSite: 'strict' });
        resp.locals.payload.username = username;
        resp.locals.permission = await db.userRole(resp.locals.payload.username);
        const message = ' ';
        const students = await db.allStudents(); 
        resp.render('main_page',{students, message});
      } else {
        resp.status(401).send('Wrong password!');
      }
    } else {
      resp.status(401).send('Wrong username!');
    }
  } else {
    resp.status(401).send('Error!');
  }
});

router.post('/register', async (req, resp) => {
  const { username, password, password2 } =  req.body;
  if (username && password && password2) {
    if (password === password2) {
      const user = await db.passwordByUsername(username);
      if (!user) {
        const hashedPassword = await bcrypt.hash(password, bcrypt.genSaltSync(10));
        await insertDb.insertUserUser(username, hashedPassword);
        const token = jwt.sign({ username }, SECRET);
        resp.cookie('token', token, { httpOnly: true, sameSite: 'strict' });
        resp.locals.payload.username = username;
        resp.locals.permission = await db.userRole(resp.locals.payload.username);
        const message = '';
        const students = await db.allStudents(); 
        resp.render('main_page',{students, message});
      } else {
        const message = 'Username already exist!';
        resp.status(401).render('auth', { message });
      }
    } else {
      const message = 'The two password is not matching!';
      resp.status(401).render('auth', { message });
    }
  } else {
    const message = 'There is no username or password!';
    resp.status(401).render('auth', { message });
  }
});

router.post('/logout', async (req, resp) => {
  resp.clearCookie('token');

  resp.locals.payload = '';
  resp.locals.permission = '';
  const message = ' ';
  const students = await db.allStudents(); 
  resp.render('main_page',{students, message});
});

router.get('/', async (req, resp) => {
  try {
    const message = '';
    resp.render('auth', { message });
  } catch (err) {
    resp.status(500).send(`Error : ${err.message}`);
  }
});

export default router;
